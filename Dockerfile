FROM caddy:2.4.6
RUN adduser -D caddy && apk --no-cache add ca-certificates curl
COPY --chown=caddy ./site/ /usr/share/caddy/
USER caddy