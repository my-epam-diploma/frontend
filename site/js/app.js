const requestButton = document.querySelector(".reqbtn");
const updateButton = document.querySelector(".updbtn");
let date = document.querySelector('input[type="date"]');

requestButton.addEventListener('click', requestDataByDate);
updateButton.addEventListener('click', updateRemoteDB);

function requestDataByDate() {
    var createTable = document.getElementById('fillTableOfDeaths')
    createTable.innerHTML = "";
    fetch(`api/v1/bydate/${date.value}`)
        .then(response => response.json())
        .then(data => {      
            for (var i = 0; i < data.data.length; i++){
                var row = `<tr>
                                <td >${data.data[i].deaths}</td>
                                <td>${data.data[i].Confirmed}</td>
                                <td>${data.data[i].country_code}</td>
                                <td>${data.data[i].stringency}</td>
                          </tr>`

                createTable.innerHTML += row
                new Tablesort(document.getElementById("tableofdeaths"), {
                    descending: false
                  });
            };
        })
};

function updateRemoteDB() {
    var createTable = document.getElementById('fillNoDataTable')
    createTable.innerHTML = "";
    fetch("/api/v1/update")
        .then(response => response.json())
        .then(data => {    
            for (var i = 0; i < data.no_data.country.length; i++){
                var row = `<tr>
                                <td>${data.no_data.country[i]}</td>
                                <td>${data.no_data.date[i]}</td>
                          </tr>`

                createTable.innerHTML += row
            };
        })
};

